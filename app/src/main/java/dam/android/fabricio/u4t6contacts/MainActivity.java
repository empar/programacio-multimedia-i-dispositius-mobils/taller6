package dam.android.fabricio.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener  {

    MyContacts myContacts;
    RecyclerView recyclerView;
    LinearLayout informacion;
    private TextView tvResult;

    private static  String [] PERMISSIONS_CONTACTS ={Manifest.permission.READ_CONTACTS};

    private  static  final  int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();


        if(checkPermissions())
            setListAdapter();

    }


    private void setUI() {
        informacion = findViewById(R.id.informacion);
        tvResult = findViewById(R.id.tvResult);
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);


        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                informacion.setVisibility(View.INVISIBLE);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();

        setListAdapter();
    }

    private void setListAdapter() {
        myContacts = new MyContacts(this);

        recyclerView.setAdapter(new MyAdapter(myContacts,this));

        if(myContacts.getCount() >0) {

           findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
        }

    }

    private  boolean checkPermissions(){

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
      return false;
        }else
            return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CONTACTS){

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setListAdapter();
            else
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
        }else

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onItemClick(Contact contact) {
        informacion.setVisibility(View.VISIBLE);
        String message = contact.toString();

        tvResult.setText(message);
    }

    @Override
    public void onItemLongClick(Contact contact) {
        informacion.setVisibility(View.INVISIBLE);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contact.getRawContac()));
        intent.setData(uri);
        startActivity(intent);
    }
}