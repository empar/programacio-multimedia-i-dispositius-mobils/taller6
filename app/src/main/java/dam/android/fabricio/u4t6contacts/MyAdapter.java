package dam.android.fabricio.u4t6contacts;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private MyContacts myContacts;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Contact contact);
        void onItemLongClick(Contact contact);
    }


    static  class  MyViewHolder extends  RecyclerView.ViewHolder{
        ConstraintLayout constraintLayout;
        TextView tvId;
        TextView tvName;
        TextView tvPhone;
        ImageView imageContact;

        public MyViewHolder(@NonNull ConstraintLayout constraintLayout) {
            super(constraintLayout);
            this.constraintLayout = constraintLayout;

            this.tvId = constraintLayout.findViewById(R.id.tvId);
            this.tvName = constraintLayout.findViewById(R.id.tvName);
            this.tvPhone = constraintLayout.findViewById(R.id.tvPhone);
            this.imageContact = constraintLayout.findViewById(R.id.imageContact);
        }

        //Crear el objeto con todos los datos

        public void bind(final Contact contact, final OnItemClickListener listener){
            this.tvId.setText(contact.getId());
            this.tvName.setText(contact.getName());
            this.tvPhone.setText(contact.getNumber());

            //compruebo si tiene imagen y se la meto
            Uri imagen = contact.getPhoto();
            if (imagen != null){
                this.imageContact.setImageURI(contact.getPhoto());
            }
            this.constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("CLICK", "NORMAL CLICK");
                    listener.onItemClick(contact);
                }
            });

            this.constraintLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    Log.i("CLICK", "LONG CLICK");
                    listener.onItemLongClick(contact);
                    return true;
                }
            });
        }
    }
    MyAdapter(MyContacts myContacts, OnItemClickListener listener){
        this.myContacts = myContacts;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ConstraintLayout constraintLayout = (ConstraintLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new MyViewHolder(constraintLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myContacts.getContactData(position), listener);
    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }

}
