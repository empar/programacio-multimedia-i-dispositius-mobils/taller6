package dam.android.fabricio.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.ImageView;

import java.util.ArrayList;


public class MyContacts {
    private ArrayList<Contact> myDataSet;
    private Context context;
    public String urlPhoto;
    ImageView img;



    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();


    }

    private ArrayList<Contact> getContacts() {
        ArrayList<Contact> ListContact = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI,


        };


        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projection, selectionFilter, null,
                ContactsContract.Data.DISPLAY_NAME + " ASC");

        if (contactsCursor != null) {
            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone._ID);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int contacIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookupIndex = contactsCursor.getColumnIndexOrThrow( ContactsContract.Data.LOOKUP_KEY);
            int rawContacIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int phoneTypeIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int PhotoIndex = contactsCursor.getColumnIndexOrThrow( ContactsContract.Data.PHOTO_THUMBNAIL_URI);

            while (contactsCursor.moveToNext()) {
               int id = contactsCursor.getInt(idIndex);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                int contac = contactsCursor.getInt(contacIndex);
                String lookup = contactsCursor.getString(lookupIndex);
                int rawContac = contactsCursor.getInt(rawContacIndex);
                String phoneType = contactsCursor.getString(phoneTypeIndex);
                Uri photo =null;

                if (contactsCursor.getString(PhotoIndex) != null){
                    photo = Uri.parse(contactsCursor.getString(PhotoIndex));
                }
                ListContact.add(new Contact(id,name,number,rawContac,phoneType,photo,contac,lookup));


            }

            contactsCursor.close();
        }
        return ListContact;
        //return contactsList;

    }

    public Contact getContactData(int position) {
        return myDataSet.get(position);
    }

    public int getCount() {
        return myDataSet.size();
    }

}
