package dam.android.fabricio.u4t6contacts;

import android.net.Uri;

public class Contact {
    private int id;
    public String name;
    public String number;
    public int rawContac;
    public String PhoneType;
    public Uri photo;
    private int contactId;
    private String lookupKey;


    public Contact(int id, String name, String number, int rawContac, String phoneType, Uri photo, int contac, String lookupKey) {
        this.id= id;
        this.name = name;
        this.number = number;
        this.rawContac = rawContac;
        this.PhoneType = phoneType;
        this.photo = photo;
        this.contactId = contac;
        this.lookupKey = lookupKey;
    }

    public String getId() {
        return "Id: "+id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getRawContac() {
        return rawContac;
    }

    public void setRawContac(int rawContac) {
        this.rawContac = rawContac;
    }

    public String getPhoneType() {
        return PhoneType;
    }

    public void setPhoneType(String phoneType) {
        PhoneType = phoneType;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public String getLookupKey() {
        return lookupKey;
    }

    public void setLookupKey(String lookupKey) {
        this.lookupKey = lookupKey;
    }
    public String getStringTypePhone() {
        String type = "";
        switch (this.PhoneType) {
            case "1":
                type = "HOME";
                break;
            case "2":
                type = "MOBILE";
                break;
            case "3":
                type = "WORK";
                break;
            default:
                type = "OTHER";
                break;
        }
        return type ;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", rawContac=" + rawContac +
                ", PhoneType='" + PhoneType + '\'' +
                ", photo=" + photo +
                ", contactId=" + contactId +
                ", lookupKey='" + lookupKey + '\'' +
                '}';
    }
}
